﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExampleUnitTests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleUnitTests.Tests
{
    [TestClass()]
    public class CalculatorTests
    {
        [TestMethod()]
        public void AddTwoNumbersTest()
        {
            // Arrange
            double x = 8.0;
            double y = 3.2;
            Calculator calculator = new Calculator();
            double expected = 11.2;

            // Act
            double result = calculator.AddTwoNumers(x, y);

            // Assert
            Assert.AreEqual(expected, result);

        }

        [TestMethod()]
        public void SubtractTwoNumbersTest()
        {
            // Arrange
            double x = 8.0;
            double y = 3.2;
            Calculator calculator = new Calculator();
            double expected = 4.8;

            // Act
            double result = calculator.SubtractTwoNumbers(x, y);

            // Assert
            Assert.AreEqual(expected, result);
        }
    }
}